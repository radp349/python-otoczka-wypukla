import matplotlib.pyplot as plt
import numpy as np
import math
import random

class point:
    def __init__(self, px, py):
        self.px = px
        self.py = py

#  Tworzę klasę tygrys o atrybutach będących koordynatami na płaszczyźnie 
class Tygrys:
    
    def __init__(self, px, py):
        self.px = px
        self.py = py
        self.radius_A = random.randint(2, 10)
        self.radius_B = random.randint(2, 10)
        self.how_many = 72
        self.border = []
        for i in range(self.how_many+1):
            x = self.radius_A*math.cos(math.radians(i*360/self.how_many))
            y = self.radius_B*math.sin(math.radians(i*360/self.how_many))
            self.border.append(point(x+self.px,y+self.py))
    
    def plot_ellipse(self):

        xx = []
        yy = []

        for p in self.border:
            xx.append(p.px)
            yy.append(p.py)
        plt.plot(xx, yy, 'r')
            

        


# Aby uniknąć sytuacji gdzie tworzenie 20 punktów (obiektów) zajmie mi 60 linii bezmyślnie skopiowanego kodu, tworzę listę punktów w pętli for
# Oczywiście współrzędne punktów są losowe
Lista=[]
Listas=[]
for i in range(0, 20): 
    x=np.random.uniform(0, 100)
    y=np.random.uniform(0, 100)
    Lista.append (Tygrys(x,y))
    Listas.append (Tygrys(x,y))
    Lista[i].plot_ellipse()
    
for i in range(0, 20): 
    for satelita in Lista[i].border:
        Lista.append(satelita)
    

    

# Sortuję Listę względem koordynatu X od najmniejszego do największego
Lista.sort(key = lambda p: (p.px))


# Deklauję funkcję do sprawdzania, cy w kolejnych krokach algorytmu Jervisa poruszamy się w lewo czy w prawo 
def Czy_lewo(tygrys1,tygrys2,tygrys3):
    return ((tygrys2.px - tygrys1.px)*(tygrys3.py - tygrys1.py) - (tygrys2.py - tygrys1.py)*(tygrys3.px - tygrys1.px)) > 0


# Tworzymy nową listę dla punktów należących do otoczki wypukłej 
Otoczka = []
# Punkt z najmniejszą współrzędną X zostaje punktem startowym
start = Lista[0]

# Implementuję algorytm Jervisa (Próbowałem użyć wcześniej algorytmu Grahama ale po kilku godzinach nieudanych prób, zdecydowałem się na zmianę)
zmienna = 0
while(True):
    # Z każdym przjeściem pętli dodajemy do otoczki punkt, który był skrajnie po lewej 
    Otoczka.append(start)
    koniec = Lista[0]
    # Bierzemy odcineh cignący się od naszego punktu końcowego do naszego punktu chwilowego. Następnie przeszukujemy listę wszystkich punktów i szukamy takiego który byłby położony na lewo od naszego odcinka
    # Jeśli taki znajdziemy, to jest to nasz nowy punkt chwilowy. Punkt chwilowy dla którego powyższe przeszukiwanie nie przynosi rezultatów należy do otoczki 
    for j in range(len(Lista)):
        if(koniec == start or Czy_lewo(Otoczka[zmienna], koniec ,Lista[j])):
            koniec = Lista[j]
    zmienna+=1
    start = koniec

    if(koniec == Otoczka[0]):
        break

# Wyrysowuję wszystkie tygrysy jako małe zielone kropki
for i in range(0, 20): 
    plt.scatter(Listas[i].px, Listas[i].py, s=50,c='green')

# Rysuję Czerowne Linie między punktami należacymi do otoczki
for i in range(0, len(Otoczka)-1): 
    plt.plot([Otoczka[i].px,Otoczka[i+1].px],[Otoczka[i].py,Otoczka[i+1].py], c='purple',linewidth=3.0)
plt.plot([Otoczka[0].px,Otoczka[len(Otoczka)-1].px],[Otoczka[0].py,Otoczka[len(Otoczka)-1].py], c='purple',linewidth=3.0)
plt.show()

