# README

## Projekt: Wizualizacja Otoczki Wypukłej na Płaszczyźnie 2D

Ten projekt generuje zestaw losowych punktów na płaszczyźnie 2D (przedstawiających "Tygrysy"), każdy z określoną elipsą wokół niego. Następnie, z wykorzystaniem algorytmu Jervisa, oblicza i wyświetla otoczkę wypukłą dla tego zestawu punktów.

### Wymagania

Do uruchomienia tego projektu potrzebujesz:

- Python 3.7+
- matplotlib 3.1+
- numpy 1.16+

### Jak korzystać?

1. Uruchom skrypt Pythona. Nie są wymagane żadne dane wejściowe.
2. Skrypt wygeneruje losowo rozmieszczone punkty (Tygrysy), każdy z elipsą wokół siebie.
3. Następnie skrypt obliczy otoczkę wypukłą dla tych punktów i narysuje ją na wykresie.
4. Wynik jest wyświetlany jako wykres z punktami, elipsami i otoczką wypukłą.

### Opis działania

Algorytm generuje 20 losowych punktów na płaszczyźnie i dla każdego punktu tworzy elipsę. Następnie punkty są sortowane według współrzędnej X i za pomocą algorytmu Jervisa tworzona jest otoczka wypukła dla tych punktów.

Wszystkie punkty (Tygrysy) są reprezentowane jako zielone kropki, elipsy są czerwone, a otoczka wypukła jest rysowana jako fioletowa linia.

Algorytm Jervisa, używany do tworzenia otoczki wypukłej, działa poprzez iteracyjne dodawanie punktów do otoczki - w każdym kroku wybiera się taki punkt, dla którego wszystkie inne punkty są po prawej stronie linii łączącej aktualny punkt otoczki i wybrany punkt. 

### Informacje dodatkowe

Proszę pamiętać, że punkty (Tygrysy) i ich elipsy są generowane losowo, więc za każdym razem wynik będzie różny. Również ze względu na losowość, niektóre elipsy mogą się na siebie nakładać.
